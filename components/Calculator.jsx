"use client"

import { useEffect, useState } from "react";
import { signOut, useSession } from 'next-auth/react';
import fetchData from '../services/fetchData';
import '../styles/Calc.css';

export default function Calculator() {
  const { data:session, status } = useSession();
  const [ calculations, setCalculations ] = useState(null);
  const [ currentValue, setCurrentValue ] = useState('0');
  const [ firstValue, setFirstValue ] = useState('');
  const [ display, setDisplay ] = useState('0');
  const [ firstValueSign, setFirstValueSign ] = useState('');
  const [ operator, setOperator ] = useState('');

  useEffect(()=> {
    getAllCalculations();
  }, [status]);

  const getAllCalculations = async () => {
    if(status) {
      const id = session?.user?.id;
      const userID = JSON.stringify({userID: id});
      const allCalculations = await fetchData(userID, '/api/get-calculations', 'post');

      const { calculations } = allCalculations;
      setCalculations(calculations);
    }
  }

  const addCalculation = async (userID, expression, result) => {
    const sendData = JSON.stringify({userID, equation: expression, result});
    const fetchResult = await fetchData(sendData, '/api/add-calculation', 'post');

    if(fetchResult.errorMessage) {
      console.log(fetchResult.errorMessage);
    } else {
      getAllCalculations();
    }
  }

  const handleOperatorButton = (inputValue) => {
    if(!firstValue && currentValue === '0') {
      if(inputValue === '+') {
        setFirstValueSign('');
        return;
      } 
      if(inputValue === '-') {
        setFirstValueSign('-');
        return;
      }
    }
    
    if(!operator) {
      setOperator(inputValue);
      setFirstValue(firstValueSign + currentValue);
      setCurrentValue('0');
      setDisplay(firstValueSign + currentValue);
      return;
    }

    if(operator && currentValue !== '0') {
      setOperator(inputValue);
      getResult();
      return;
    }

    if(operator && currentValue === '0') {
      setOperator(inputValue);
      return;
    }
  }

  const handleClearButton = () => {
    clearValues('all');
  }

  const handleCancelButton = () => {
    setCurrentValue('0');
    setDisplay('0');
  }

  const handleDotButton = () => {
    if (currentValue.includes(".")) return;

    setCurrentValue(currentValue + '.');
    setDisplay(currentValue);
  }

  const handlePercentageButton = (inputValue) => {
    const userID = session?.user?.id;
    let calculationResult;
    let expression;

    if(!firstValue && !operator) {
      return;
    }

    calculationResult = calculate(Number(firstValue), inputValue, Number(currentValue));
    expression = `${ firstValue } ${ operator } ${ currentValue } %`;
    
    clearValues('all');
    setDisplay(calculationResult.toString());

    addCalculation(userID, expression, `${ calculationResult } %`);
  }

  const handleEqualButton = () => {
    if(firstValue && operator) {
      getResult(); 
      return;
    } else return;
  }

  const handleNumberButton = (inputValue) => {
    let newValue;

    if(currentValue !== '0') newValue = currentValue + inputValue;
    else newValue = inputValue;

    setCurrentValue(newValue);
    setDisplay(newValue);
  }

  const getResult = () => {
    const userID = session?.user?.id;
    let calculationResult;
    let expression;

    if(!firstValue && !operator) return;

    if(firstValue && operator && currentValue === '0') {
      calculationResult = calculate(Number(firstValue), operator, Number(firstValue));
      expression = `${ firstValue } ${ operator } ${ firstValue } `;
    } 
    
    if(firstValue && operator && currentValue !== '0' ) {
      calculationResult = calculate(Number(firstValue), operator, Number(currentValue));
      expression = `${ firstValue } ${ operator } ${ currentValue }`;
    } 

    calculationResult = +calculationResult.toFixed(5).toString();

    setDisplay(calculationResult.toString());
    setFirstValue(calculationResult.toString());
    addCalculation(userID, expression, calculationResult);
    clearValues();
  }

  const calculate = (x, operator, y) => {
    switch(operator) {
      case '+': 
        return x + y;
      case '-': 
        return x - y;
      case '*': 
        return x * y;
      case '/': 
        return x / y;
      case '%': 
        return x / 100 * y;
    }
  }

  const clearValues = (option) => {
    if (option === 'all') {
      setFirstValue('');
      setCurrentValue('0');
      setFirstValueSign('');
      setOperator('');
      setDisplay('0');
    } else {
      setCurrentValue('0');
      setFirstValueSign('');
    }
  }

  const handleButtonClick = (e) => {
    let inputValue = e.target.dataset.val.toString();

    switch (inputValue) {
      case '+':
      case '-':
      case '*':
      case '/':
        handleOperatorButton(inputValue);
        return;
      case 'clear':
        handleClearButton();
        return;
      case 'cancel':
        handleCancelButton();
        return;
      case '.':
        handleDotButton();
        return;
      case '%':
        handlePercentageButton(inputValue);
        return;
      case "=":
        handleEqualButton();
        return;
      case '0':
        if (currentValue === "0") return;
      default: 
        handleNumberButton(inputValue);
    }
  }

  return (
    <>
      <div className="menu">
        <p>{ session?.user?.name }</p>
        <button className="logout-btn" onClick={() => signOut() }>Log Out</button>
      </div>
      <div className="calc-body">
        <div className="calc-screen">{ display !== '0' ? display : currentValue }</div>
        <div className="digits">
          <button className="digit-button" data-val="9" onClick={ handleButtonClick }>9</button>
          <button className="digit-button" data-val="8" onClick={ handleButtonClick }>8</button>
          <button className="digit-button" data-val="7" onClick={ handleButtonClick }>7</button>
          <button className="digit-button" data-val="6" onClick={ handleButtonClick }>6</button>
          <button className="digit-button" data-val="5" onClick={ handleButtonClick }>5</button>
          <button className="digit-button" data-val="4" onClick={ handleButtonClick }>4</button>
          <button className="digit-button" data-val="3" onClick={ handleButtonClick }>3</button>
          <button className="digit-button" data-val="2" onClick={ handleButtonClick }>2</button>
          <button className="digit-button" data-val="1" onClick={ handleButtonClick }>1</button>
          <button className="sign-button" data-val="%" onClick={ handleButtonClick }>%</button>
          <button className="sign-button" data-val="." onClick={ handleButtonClick }>.</button>
          <button className="digit-button" data-val="0" onClick={ handleButtonClick }>0</button>
        </div>
        <div className="signs">
          <button className="sign-button" data-val="+" onClick={ handleButtonClick }>+</button>
          <button className="sign-button" data-val="-" onClick={ handleButtonClick }>-</button>
          <button className="sign-button" data-val="*" onClick={ handleButtonClick }>*</button>
          <button className="sign-button" data-val="/" onClick={ handleButtonClick }>/</button>
        </div>
        <div className="controlls">
          <button className="ac-button" data-val={ display === '0' ? 'clear' : 'cancel' } 
            onClick={ handleButtonClick }>{ display === '0' ? 'AC' : 'C' }</button>
          <button className="equal-button" data-val="=" onClick={ handleButtonClick }>=</button>
        </div>
      </div>
      <ul className="results-list">
        {calculations && calculations.map(({ _id, equation, result }) => (
          <li key={ _id }>{ equation } = { result }</li>
        ))}
      </ul>
    </>
  )
};
