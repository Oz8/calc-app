"use client"

import { useState } from "react";
import Link from "next/link";
import { signIn } from 'next-auth/react';
import { useRouter } from "next/navigation";

import '../styles/Forms.css';

export default function LoginForm() {

  const [name, setName] = useState('');
  const [password, setPassword] = useState('');
  const [nameError, setNameError] = useState('');
  const [passwordError, setPasswordError] = useState('');

  const router = useRouter();

  const submitForm = async (e) => {
    e.preventDefault();

    if(!name || name.length < 4 || name.length > 20) {
      setNameError('Required username length 4-20 symbols');
      return;
    } else {
      setNameError('');
    }

    if(!password || password.length < 6 || password.length > 20) {
      setPasswordError('Required password length 6-20 symbols');
      return;
    } else {
      setPasswordError('');
    }

    try {
      const res = await signIn('credentials', {
        name: name, password: password, redirect: false
      });

      if(res.error) {
        setPasswordError('Invalid Credentials');
        return;
      }

      router.replace('calc');
    } catch (error) {
      console.log('Errors: ', error);
    }
  }

  return (
    <div className="form-cover">
      <h1>Login</h1>
      <form action="#" method="post" onSubmit={ submitForm }>
        <input type="text" placeholder="username" value={ name } onChange = {(e) => setName(e.target.value)} />
        { nameError && (
          <p className="error">{ nameError }</p>
        )}
        <input type="password" placeholder="password" value={ password } onChange = {(e) => setPassword(e.target.value)} />
        { passwordError && (
          <p className="error">{ passwordError }</p>
        )}
        <input type="submit" value="Login" />
      </form>
      <Link className="link" href="/signup">Create an account</Link>
    </div>
  )
}