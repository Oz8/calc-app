"use client"

import { useEffect, useState } from "react";
import { signOut, useSession } from 'next-auth/react';
import fetchData from '../services/fetchData';
import '../styles/Calc.css';

export default function Calculator() {
  const { data:session, status } = useSession();
  const [ calculations, setCalculations ] = useState(null);
  const [ expression, setExpression ] = useState('');
  const [ result, setResult ] = useState('0');

  useEffect(()=> {
    getAllCalculations();
  }, [status]);

  const getAllCalculations = async () => {
    if(status) {
      const id = session?.user?.id;
      const userID = JSON.stringify({userID: id});
      const allCalculations = await fetchData(userID, '/api/get-calculations', 'post');

      const { calculations } = allCalculations;
      setCalculations(calculations);
    }
  }

  const addCalculation = async (userID, expression, result) => {
    const sendData = JSON.stringify({userID, equation: expression, result});
    const fetchResult = await fetchData(sendData, '/api/add-calculation', 'post');

    if(fetchResult.errorMessage) {
      console.log(fetchResult.errorMessage);
    } else {
      getAllCalculations();
    }
  }

  const addToExpression = (e) => {
    let newValue;
    let value = e.target.dataset.val.toString();
    
    if(expression.length > 0 || value != '0') {
      newValue = expression + value;
      setExpression(newValue);
      setResult(newValue);
    }
  }

  const getResult = () => {
    if(expression) {
      const geval = (expression) => {
        return eval?.(`"use strict";(${expression})`);
      }
      try {
        let result = geval(expression);
        if(result) {
          setResult(result);
          updateData(expression, result);
        } else {
          clearResult();
        }
      } catch (e) {
        if (e instanceof SyntaxError) {
          setResult('Malformed expression');
          setExpression('');
        }
      }
    }
  }

  const updateData = (expression, result) => {
    const userID = session?.user?.id;
    addCalculation(userID, expression, result);
  }

  const clearResult = () => {
    setResult('0');
    setExpression('');
  }

  return (
    <>
      <div className="menu">
        <p>{ session?.user?.name }</p>
        <button className="logout-btn" onClick={() => signOut() }>Log Out</button>
      </div>
      <div className="calc-body">
        <div className="calc-screen">{ result }</div>
        <div className="digits">
          <button className="digit-button" data-val="9" onClick={ addToExpression }>9</button>
          <button className="digit-button" data-val="8" onClick={ addToExpression }>8</button>
          <button className="digit-button" data-val="7" onClick={ addToExpression }>7</button>
          <button className="digit-button" data-val="6" onClick={ addToExpression }>6</button>
          <button className="digit-button" data-val="5" onClick={ addToExpression }>5</button>
          <button className="digit-button" data-val="4" onClick={ addToExpression }>4</button>
          <button className="digit-button" data-val="3" onClick={ addToExpression }>3</button>
          <button className="digit-button" data-val="2" onClick={ addToExpression }>2</button>
          <button className="digit-button" data-val="1" onClick={ addToExpression }>1</button>
          <button className="sign-button" data-val="." onClick={ addToExpression }>.</button>
          <button className="digit-button zero-btn" data-val="0" onClick={ addToExpression }>0</button>
        </div>
        <div className="signs">
          <button className="sign-button" data-val="+" onClick={ addToExpression }>+</button>
          <button className="sign-button" data-val="-" onClick={ addToExpression }>-</button>
          <button className="sign-button" data-val="*" onClick={ addToExpression }>*</button>
          <button className="sign-button" data-val="/" onClick={ addToExpression }>/</button>
        </div>
        <div className="controlls">
          <button className="ac-button" onClick={ clearResult }>AC</button>
          <button className="equal-button" onClick={ getResult }>=</button>
        </div>
      </div>
      <ul className="results-list">
        {calculations && calculations.map(({ _id, equation, result }) => (
          <li key={ _id }>{ equation } = { result }</li>
        ))}
      </ul>
    </>
  )
};
