async function fetchAllCalculations(data, url, method) {
  return await fetch(url,
    {
      headers: { 'content-type': 'application/json' },
      body: data,
      method
    })
    .then((response) => response.json())
}

export default fetchAllCalculations;