import mongoose, { Schema, models } from 'mongoose';

const calculationSchema = new Schema({
  userID: {
    type: String,
    require: true
  },
  equation: {
    type: String,
    required: true
  },
  result: {
    type: String,
    required: true
  }
});

const Calculation = models.Calculation || mongoose.model('Calculation', calculationSchema);

export default Calculation;