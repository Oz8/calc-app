import { connectMongoDB } from "@/lib/mongodb";
import Calculation from "@/models/calculation";
import { NextResponse } from "next/server";

export async function POST(req) {
  try {
    const { userID, equation, result } = await req.json();
    await connectMongoDB();
    await Calculation.create({userID, equation, result});

    return NextResponse.json({message: 'Calculation added'}, {status: 200});
  } catch (error) {
    return NextResponse.json({message: `Error: ${ error }`}, {status: 500});
  }
}
  