import { connectMongoDB } from '@/lib/mongodb';
import User from '@/models/user';
import NextAuth from 'next-auth/next';
import CredentialsProvider from 'next-auth/providers/credentials';
import bcrypt from 'bcryptjs';

const authOptions = {
  providers: [
    CredentialsProvider({
      name: 'credentials',
      credentials: {
        name: { label: "name", type: "text", placeholder: "jsmith" },
        password: { label: "password", type: "password" }
      },

      async authorize(credentials, req, res) {

        const { name, password } = credentials;
        
        try {
          await connectMongoDB();
          const user = await User.findOne({ name });

          if(!user) {
            return null;
          }

          const passwordMatch = await bcrypt.compare(password, user.password);

          if(!passwordMatch) {
            return null;
          }
          
          console.log(user);
          return user;

        } catch (error) {
          console.log('error connection to db ', error);
        }
      }
    })
  ],
  callbacks: {
    jwt({ token, user }) {
      if (user) {
        return { ...token, id: user.id };
      }
      return token;
    },
    session: ({ session, token, user }) => {
      return {
        ...session,
        user: {
          ...session.user,
          id: token.id,
        },
      };
    },
  },
  session: {
    strategy: 'jwt',
  },
  secret: process.env.NEXTAUTH_SECRET,
  pages: {
    signIn: '/'
  }
};

const handler = NextAuth(authOptions);

export { handler as GET, handler as POST };