import { connectMongoDB } from "@/lib/mongodb";
import Calculation from "@/models/calculation";
import { NextResponse } from "next/server";

export async function POST(req) {
  try {
    const { userID } = await req.json();
    await connectMongoDB();
    const calculations = await Calculation.find().where('userID', userID).sort({ $natural: -1 }).limit(15);
    
    return Response.json({ calculations });
  } catch (error) {
    return NextResponse.json({message: `Error: ${ error }`}, {status: 500});
  }
}
  