import { connectMongoDB } from "@/lib/mongodb";
import User from "@/models/user";
import { NextResponse } from "next/server";
import bcrypt from 'bcryptjs';

export async function POST(req) {
  try {
    const { name, password } = await req.json();
    const hashedPassword = await bcrypt.hash(password, 10);
    await connectMongoDB();
    await User.create({name, password: hashedPassword});

    return NextResponse.json({message: 'User successfuly registered'}, {status: 200});
  } catch (error) {
    return NextResponse.json({message: `Registration error ${error}`}, {status: 500});
  }
}